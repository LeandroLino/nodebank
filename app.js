const express = require('express');
const app = express()
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')
require('dotenv/config')

//Middlewares
app.use(cors())
app.use(bodyParser.json())
//Import Routes
const postsRoute = require('./routes/posts')

app.use('/api', postsRoute)



mongoose.connect(process.env.DB_CONNECTION,
    { useNewUrlParser: true, useUnifiedTopology: true,useCreateIndex: true}, () => {
        console.log('connected to Database')
})
//How to we start listening to the server
app.listen(3000)