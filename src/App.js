import React from 'react';
import './App.css';

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={"https://pietroski.gitlab.io/pytroski/Roma_Aeterna.jpg"} className="App-logo" alt="logo" />
        <p>Written by&nbsp;
        <a href="https://www.linkedin.com/in/augusto-pietroski/" rel="noreferrer noopener" target="_blank" >Augusto Pietroski</a>
        </p>
      </header>
    </div>
  );
}

export default App;
