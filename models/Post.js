const mongoose = require('mongoose')


const PostSchema = mongoose.Schema({
    cpf: {
        type: String,
        require: true,
        unique: true
    },
    value: {
        type: Number,
        require: true,
    },
    date: {
        type: Date,
        default: Date.now
    },
    balance: {
        type: Number,
        default: 0
    },
    type: {
        type: String,
        require: true
    },
})

module.exports = mongoose.model('Posts', PostSchema)