# NodeBank

NodeBank, Projeto criado para Teste Técnico, uma registradora de situações Bancárias.

## Instalação

Antes de baixar o Back end, crie o Banco de Dados, no site [link1](https://cloud.mongodb.com/),

(Talvez seja necessário criar uma Organização, mas é tudo gratuito)crie um banco de dados e uma cluster,

após criar uma cluster, clique em `Connect` e depois em `Connect using MongoDB Compass` ele vai gerar um

link semelhante á este `mongodb+srv://<seu nome>:<password>@cluster0.gbw2k.mongodb.net/` você pode ir 

até o .env example e substituir o link por este, usando seu nome e password (Talvez seja necessário 

criar um user para poder acessar a URL) feito isso, remove o ` example` do nome do arquivo, e pode seguir

com o npm.

Após baixar todo o arquivo de Back end e criar o banco de dados, rode primeiro o 

```node
npm install
```
Em seguinda basta iniciar a conexão
```node
npm start
```
A resposta dada no terminal será:
```
[nodemon] 2.0.7
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node app.js`
connected to Database

```
## Utilização
Uma RestAPI de controle Bancário,


Como a API está hospedada localmente, usaremos rotas de local hosto com a porta 3000
## Problmas possiveis

```bash
Error: listen EADDRINUSE: address already in use :::3000
```

Tente verificar se existe alguma outra aplicação rodando na porta 3000, caso não consiga achar nenhuma
aplicação rodando utilize os seguintes comandos

```bash
fuser 3000/tcp
```

A resposta desse comando será:

```bash
3000/tcp:            <Sequencia Númerica>
```

Depois de capturar a Sequencia númerica utilize o seguinte comando:
```bash
kill -9 <Sequencia Númerica>
```

Pronto, será finalizado a aplicação e agora você pode rodar o comando npm novamente

Se o problema persistir você pode trocar o valor `3000` para algum numero como `5000`

Se mesmo assim continuar, você pode me contactar pelo email: leao.lino7@gmail.com, ou pelo meu 
celular (11) 940596713


## POST /api/deposit/ - Executando um Depósito:
Não utilize números negativos, apenas numeros.
```
{
  "cpf": <Cpf Válido, apenas 11 digitos>,
  "value": 5433
}
```
**RESPONSE STATUS -> HTTP 200 - OK**
```
{
  "balance": <Saldo Bancário>,
  "_id": <Id Gerado Automaticamente>,
  "cpf": <Cpf Válido, apenas 11 digitos>,
  "value": 5433,
  "type": "deposit",
  "date": <Data gerada no momento do Depósito>,
}
```
## POST /api/withdraw/ - Executando um saque:
Não utilize números negativos, apenas numeros.
```
{
  "cpf": <Cpf Válido, apenas 11 digitos>,
  "value": 201
}
```
**RESPONSE STATUS -> HTTP 200 - OK**
```
{
  "balance": <Saldo Bancário>,
  "_id": <Id Gerado Automaticamente>,
  "cpf": <Cpf Válido, apenas 11 digitos>,
  "value": -201,
  "type": "withdraw",
  "date": <Data gerada no momento do Depósito>,
}
```

## POST /api/extract/ - Executando um saque:
Não utilize números negativos, apenas numeros.
```
{
  "cpf": <Cpf Válido, apenas 11 digitos>,
}
```
**RESPONSE STATUS -> HTTP 200 - OK**
```
{
  "activities": [
    {
      "balance": <saldo>,
      "_id": <Id Gerado Automaticamente>,
      "cpf": <Cpf Válido, apenas 11 digitos>,
      "value": <Valor da transação>,
      "type": <Tipo da transação>,
      "date": <Data da transferencia>,
    },
    {
      "balance": <saldo>,
      "_id": <Id Gerado Automaticamente>,
      "cpf": <Cpf Válido, apenas 11 digitos>,
      "value": <Valor da transação>,
      "type": <Tipo da transação>,
      "date": <Data da transferencia>,
    },
    {
      "balance": <saldo>,
      "_id": <Id Gerado Automaticamente>,
      "cpf": <Cpf Válido, apenas 11 digitos>,
      "value": <Valor da transação>,
      "type": <Tipo da transação>,
      "date": <Data da transferencia>,
    },
    {
      "balance": <saldo>,
      "_id": <Id Gerado Automaticamente>,
      "cpf": <Cpf Válido, apenas 11 digitos>,
      "value": <Valor da transação>,
      "type": <Tipo da transação>,
      "date": <Data da transferencia>,
    },
    {
      "balance": <saldo>,
      "_id": <Id Gerado Automaticamente>,
      "cpf": <Cpf Válido, apenas 11 digitos>,
      "value": <Valor da transação>,
      "type": <Tipo da transação>,
      "date": <Data da transferencia>,
    },
  ],
  "balance": {
    "cpf": <Cpf Válido, apenas 11 digitos>,
    "balance": <Saldo>
  }
}
```
## POST /api/transfer/ - Efetuando uma Transferência:
```
{
  "from_cpf": <Cpf Válido, apenas 11 digitos>,
  "to_cpf": <Cpf Válido, apenas 11 digitos>,
  "value": 100
}
```
**RESPONSE STATUS -> HTTP 200**
```
[
  {
    "balance": <Saldo Bancário>,
    "_id": <Id Gerado Automaticamente>,
    "cpf": <Cpf de quem recebe o valor>,
    "value": 100,
    "type": "transfer",
    "date": <Data gerada no momento do Depósito>,
  },
  {
    "balance": <Saldo Bancário>,
    "_id": <Id Gerado Automaticamente>,
    "cpf": <Cpf de quem atribui o valor>,
    "value": -100,
    "type": "transfer",
    "date": <Data gerada no momento do Depósito>,
  }
]
```
## Observações

Durante o processo de criação eu tive muitas ideias de como melhorar o projeto, pretendo continuar 

desenvolvendo algumas features, uma coisa que eu senti falta foi a authenticação, acredito que usuarios 

poderiam criar contas com base em seu cpf e ir efetuando pagamentos somente quando logados, para evitar 

roubos ou danos.

Acredito que o projeto poderia ser bem mais estruturado com status de retorno, como quando fossemos

puxar o extrato de algum cpf que ainda não existe registro, e também tipos de operações como cheques, 

crédito e empréstimos.

Uma outra ideia que acho válido é de criar um dark mode (modo escuro) já que toda a paleta foi feita
com cores bem claras.
