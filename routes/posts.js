const express = require('express')
const router = express.Router()
const Post = require('../models/Post')


//GET BACK ALL THE POSTS
router.post('/extract', async (req, res) => {
    try{
        const posts = await Post.find({cpf: req.body.cpf})
        res.json({activities:posts, balance: {
            cpf: posts[posts.length-1].cpf,
            balance: posts[posts.length-1].balance}})
    }catch(err){
        res.json({message:err})
    }
})

//SUBMITS A POST
router.post('/deposit', async (req, res) => {
    const post_to_add = await Post.find({cpf: req.body.cpf})
    const balance = post_to_add.length > 0 ? post_to_add[post_to_add.length-1].balance : 0
    const post = new Post({
        cpf: req.body.cpf,
        value: "+"+req.body.value,
        balance: balance ? balance + req.body.value : req.body.value,
        type: "deposit",
    })
    
    try{
        const savedPost = await post.save()
        res.json(savedPost)
    } catch(err){
        res.json({message: err})
    }
})

router.post('/withdraw', async (req, res) => {
    const post_to_remove = await Post.find({cpf: req.body.cpf})
    const balance = post_to_remove.length > 0 ? post_to_remove[post_to_remove.length-1].balance : 0

    const post = new Post({
        cpf: req.body.cpf,
        value: "-"+req.body.value,
        balance: balance - req.body.value,
        type: "withdraw",
    })

    try{
        const savedPost = await post.save()
        res.json(savedPost)
    } catch(err){
        res.json({message: err})
    }
})

router.post('/transfer', async (req, res) => {
    const post_to_add = await Post.find({cpf: req.body.to_cpf})
    const balance_to_add = post_to_add.length > 0 ? post_to_add[post_to_add.length-1].balance : 0
    
    const post_to_remove = await Post.find({cpf: req.body.from_cpf})
    const balance_to_remove = post_to_remove.length > 0 ? post_to_remove[post_to_remove.length-1].balance : 0
    
    const way_to_add = new Post({
        cpf: req.body.to_cpf,
        value: "+"+req.body.value,
        balance: balance_to_add ? balance_to_add + req.body.value : req.body.value,
        type: "transfer",
    })
    const way_to_from = new Post({
        cpf: req.body.from_cpf,
        value: `-`+req.body.value,
        balance: balance_to_remove ? balance_to_remove - req.body.value : req.body.value,
        type: "transfer",
    })
    
    try{
        const ToSavedPost = await way_to_add.save()
        const FromSavedPost = await way_to_from.save()
        res.json([ToSavedPost,FromSavedPost])
    } catch(err){
        res.json({message: err})
    }
})
/*
//Delete Post
router.delete('/:postId', async (req, res)=>{
    try {
        const removedPost = await Post.remove({_id: req.params.postId })
        res.json(removedPost)
    }catch{
        res.json({message: err})
    }
})

//Update a post
router.patch('/:postId', async (req, res) => {
    try{
    const updatePost = await Post.updateOne({_id: req.params.postId}, 
        {$set: {title: req.body.title}})
        res.json(updatePost)
    }catch{
        res.json({message: err})
    }
})
*/
module.exports = router;
